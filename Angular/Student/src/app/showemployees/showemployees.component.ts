import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
declare var jQuery: any;


@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrl: './showemployees.component.css'
})
export class ShowemployeesComponent implements OnInit {

  employees: any;
  emailId:any;
  countries: any;
  departments: any;
  
 
 constructor(private service: EmpService) {
    this.emailId = localStorage.getItem('emailId');
  }

  ngOnInit() {
    this.service.getAllEmployees().subscribe((data: any) => {
      console.log(data);
      this.employees = data;
    });
  }
  editEmployee(emp: any) {
    
    jQuery('#empModal').modal('show');
    //alert('Edit Employee, empId: ' + emp.empId);
}
updateEmployee() {
  alert("Employee Updated...");
}

// deleteEmployee(empId:any) {
//     alert('Delete Employee, empId: ' + empId);
// }
deleteEmployee(empId:any) {
  this.service.deleteEmployee(empId).subscribe((data: any) => {console.log(data);});

  const i = this.employees.findIndex((element: any) => {
    return empId == element.empId;
  });

  this.employees.splice(i, 1);  
}
}




    // this.employees = [
    //   {empId:101, empName:'Harsha', salary:1212.12, gender:'Male',   doj:'12-13-2018', country:'IND', emailId:'harsha@gmail.com', password:'123'},
    //   {empId:102, empName:'Pasha',  salary:2323.23, gender:'Male',   doj:'11-14-2019', country:'SWD', emailId:'pasha@gmail.com',  password:'123'},
    //   {empId:103, empName:'Indira', salary:3434.34, gender:'Female', doj:'10-15-2020', country:'CHI', emailId:'indira@gmail.com', password:'123'},
    //   {empId:104, empName:'Venkat', salary:4545.45, gender:'Male',   doj:'09-16-2021', country:'NEP', emailId:'venkat@gmail.com', password:'123'},
    //   {empId:105, empName:'Gopi',   salary:5656.56, gender:'Male',   doj:'08-17-2022', country:'IND', emailId:'gopi@gmail.com',   password:'123'},
    //   {empId:106, empName:'Pavan',  salary:6767.67, gender:'Male',   doj:'07-18-2023', country:'IND', emailId:'pavan@gmail.com',  password:'123'}
    // ];

  





