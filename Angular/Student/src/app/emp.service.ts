import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmpService {
  loginStatus: boolean;
  

  constructor(private http: HttpClient) {
    this.loginStatus = false;
  }
 
  getAllCountries(): any {
    return this.http.get('http://restcountries.com/v3.1/all');
}
  getAllEmployees(): any {
    return this.http.get('http://localhost:8080/getAllEmployees');
}

getEmployeeById(empId: any): any {
  return this.http.get('http://localhost:8080/getEmployeeById/' + empId);
}


getAllDepartments(): any {
  return  this.http.get('http://localhost:8080/getAllDepartments');
}
registerEmployee(emp : any){
  return this.http.post('http://localhost:8080/addEmployee', emp);
}

deleteEmployee(empId: any) {
  return this.http.delete('http://localhost:8080/deleteEmployeeById/' + empId);
}
employeeLogin(emailId: any, password: any) {
  return this.http.get('http://localhost:8085/empLogin/' + emailId + '/' + password).toPromise();
}




  isUserLoggedIn() {
    this.loginStatus = true;
  }

  isUserLoggedOut() {
    this.loginStatus = false;
  }

  getLoginStatus(): boolean {
    return this.loginStatus;
  }

 
}

