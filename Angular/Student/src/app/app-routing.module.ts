import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ShowemployeesComponent } from './showemployees/showemployees.component';
import { ProductComponent } from './product/product.component';
import { LogoutComponent } from './logout/logout.component';
import { authGuard } from './auth.guard';
import { ShowempByIdComponent } from './showemp-by-id/showemp-by-id.component';


const routes: Routes = [
  {path:'',            component:LoginComponent},
  {path:'login',       component:LoginComponent},
  {path:'register',    component:RegisterComponent},
  {path:'showemps',    canActivate:[authGuard],component:ShowemployeesComponent},
  {path:'showempbyid', canActivate:[authGuard],component:ShowempByIdComponent},
  {path:'products',    canActivate:[authGuard],component:ProductComponent},
  {path:'logout',      canActivate:[authGuard],component:LogoutComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]

})
export class AppRoutingModule { }


