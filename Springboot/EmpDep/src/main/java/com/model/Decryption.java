package com.model;
    import javax.crypto.Cipher;
	import javax.crypto.spec.SecretKeySpec;

	public class Decryption{

	    public static String decrypt(String encryptedText, String key) throws Exception {
	        SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "AES");
	        Cipher cipher = Cipher.getInstance("AES");
	        cipher.init(Cipher.DECRYPT_MODE, secretKey);
	        byte[] decryptedTextBytes = cipher.doFinal(encryptedText.getBytes());
	        return new String(decryptedTextBytes);
	    }

	    public static void main(String[] args) {
	        try {
	            String decryptedText = decrypt("yourEncryptedText", "yourSecretKey");
	            System.out.println("Decrypted Text: " + decryptedText);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	}


	
